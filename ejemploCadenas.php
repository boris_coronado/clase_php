<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 24-04-18
 * Time: 09:24 PM
 */

/*
 * strip_tags
 */
$text = '<h1>Párrafo de prueba, </h1> <h2>Otro texto</h2>';
echo $text;
echo strip_tags($text);
//permite <p> y <a>
echo strip_tags($text, '<h1><h2>');

/*
 * strrchr
 */
echo strrchr("la malcriada","m");
echo strrchr("segunda prueba", "prueba");

$cadena = "el número 5 está presente en los números 55, 555 y 1555";
//número de ocurrencias del 5
echo "El número 5 se repite: ".substr_count($cadena, "5"). "veces<br>";
//número de ocurrencias del 5 comenzando desde el caracter 13
echo "El número 5 se repite: ".substr_count($cadena, "5", 13). "veces<br>";
//numero de ocurrencias del 5 comenzando desde el caracter 13 en los siguientes 35 caracteres



$frase = "Esta frase la vamos a dividir teniendo en cuenta los espacios blanco"

?>